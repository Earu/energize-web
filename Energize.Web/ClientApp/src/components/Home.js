import React from 'react';
import ReactDOM from 'react-dom';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';

export default class Home extends React.Component {
    displayName = Home.name;

    capitalize(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    async fetchAndDisplayChangelog() {
        let response = await fetch('https://api.github.com/repos/Earu/Energize/commits', {
            method: 'GET'
        });

        if (response.ok) {
            let commits = await response.json();
            commits = commits.slice(0, 10).map(commit => (
                <li className='commit'>
                    <a href={commit.html_url}>
                        {this.capitalize(commit.commit.message)}
                        <hr />
                        {new Date(commit.commit.author.date).toLocaleString()}
                        <img src={commit.author.avatar_url} alt='author_avatar' />
                        <span>{commit.author.login}</span>
                    </a>
                </li>
            ));
            ReactDOM.render(<ul>{commits}</ul>, document.getElementById('changelogRoot'));
        }
    }

    async fetchAndDisplayBotInformation() {
        let response = await fetch('./api/info', {
            method: 'GET'
        });

        if (response.ok) {
            let botInfo = await response.json();
            ReactDOM.render(botInfo.userCount, document.getElementById('userCount'))
            ReactDOM.render(botInfo.serverCount, document.getElementById('serverCount'))
        }
    }

    componentDidMount = () => {
        this.fetchAndDisplayChangelog();
        this.fetchAndDisplayBotInformation();
    };

    render() {
        return (
            <div className='home'>
                <video id='visualizer' src='./video/visualizer.mp4' autoPlay loop muted />
                <div className='intro'>
                    <Row className='p-0 m-0'>
                        <Col md={2} className='p-0 m-0' />
                        <Col md={8} className='p-0 m-0'>
                            <Row className='p-0 m-0'>
                                <Col md={6} className='p-0 m-0'>
                                    <img src='./img/logo.png' className='logo' alt='logo' />
                                </Col>
                                <Col md={6} className='p-0 m-0'>
                                    <span className='intro-text'>An augmented Discord experience</span>
                                    <a href='https://discordapp.com/oauth2/authorize?client_id=360116713829695489&scope=bot&permissions=0' className='invite-btn'>Invite</a>
                                    <form className="learn-more-btn" style={{ padding: '0' }} action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                        <input type="hidden" name="cmd" value="_s-xclick" />
                                        <input type="hidden" name="hosted_button_id" value="BX4VDHJLU9QYG" />
                                        <input style={{ background: 'transparent', border: 'none', height: '45px', width: '100%' }} type="submit" border="0" name="submit" value="Donate" title="PayPal - The safer, easier way to pay online!" />
                                    </form>
                                </Col>
                            </Row>
                        </Col>
                        <Col md={2} className='p-0 m-0' />
                    </Row>
                </div>
                <div className='pros-container'>
                    <Row className='pros container'>
                        <Col md={4}>
                            <div>
                                <img src='./img/volta/radio.png' alt='radio' height='150px' />
                                <br />
                                <span>Radios</span>
                                <hr />
                                <p>Take advantage of a large number of radio stations.</p>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div>
                                <img src='./img/volta/music.png' alt='music' height='150px' />
                                <br />
                                <span>Diverse Sources</span>
                                <hr />
                                <p>Search and play songs from your favorite websites, including Spotify!</p>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div>
                                <img src='./img/volta/user_friendly.png' alt='user friendly' height='150px' />
                                <br />
                                <span>User Friendly</span>
                                <hr />
                                <p>Enjoy Energize features like a Discord pro.</p>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div>
                                <img src='./img/volta/uptime.png' alt='uptime' height='150px' />
                                <br />
                                <span>Uptime</span>
                                <hr />
                                <p>Energize is accessible at any moment as long as you have a connected Discord account.</p>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div>
                                <img src='./img/volta/update.png' alt='commands' height='150px' />
                                <br />
                                <span>Commands</span>
                                <hr />
                                <p>A large number of commands to empower our users.</p>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div>
                                <img src='./img/volta/support.png' alt='support' height='150px' />
                                <br />
                                <span>Support</span>
                                <hr />
                                <p>Send us your bugs and ideas and expect a quick answer!</p>
                            </div>
                        </Col>
                    </Row>
                </div>
                <div className='stats' style={{ backgroundImage: 'url(./img/mixer.png)' }}>
                    <br /><h3 style={{ margin: 0 }}>Facts about Energize<span role='img' aria-label='thunderbolt'>⚡</span></h3>
                    <div className='container'>
                        <Row>
                            <Col md={4}>
                                <div className='stat'>
                                    <i className="fas fa-users" /><br /><span id='serverCount'>0</span> Servers
                                    </div>
                            </Col>
                            <Col md={4}>
                                <div className='stat'>
                                    <i className="fas fa-user" /><br /><span id='userCount'>0</span> Users
                                    </div>
                            </Col>
                            <Col md={4}>
                                <div className='stat'>
                                    <i className="fas fa-code" /><br /> 20K+ lines of code
                                </div>
                            </Col>
                        </Row>
                    </div>
                </div>
                <div className='changelog'>
                    <div className='container'>
                        <h3>Changelog</h3>
                        <ul id='changelogRoot' />
                    </div>
                </div>
            </div>
        );
    }
}